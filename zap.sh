mkdir reports/

mkdir /zap/wrk

/zap/zap-full-scan.py -t $target -r report.html -d -m 5

cp /zap/wrk/* reports/

returnCode=0
if grep -q "<!DOCTYPE html"> reports/report.html ; then
    echo "ok"
    returnCode=1
fi
echo $returnCode
